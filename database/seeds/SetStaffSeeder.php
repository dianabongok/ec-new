<?php

use Illuminate\Database\Seeder;

class SetStaffSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('set_staff')->insert([
            'Name' => 'DIANA BINTI NORDIN',
            'ic' => '961227-14-6358',
            'Contact_Number' => '019-2207120',
            'Emergency_Contact' => '016-2878062',
            'Address' => 'No 21 jalan KE 2/2 kota emerald 48000 RAWANG SELANGOR',
            'Worker_status'=>'intership',
            'Position'=>'none',
            'Bank_acc'=>'7052991909'

        ]);
    }
}
