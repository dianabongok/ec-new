<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSetStaff extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('set_staff', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Name');
            $table->string('ic');
            $table->string('Contact_Number');
            $table->string('Emergency_contact');
            $table->string('Address');
            $table->string('Worker_Status');
            $table->string('Position');
            $table->string('Bank_acc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('set_staff');
    }
}
