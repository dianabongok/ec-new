<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<!-- START @HEAD -->
<head>
<!-- START @META SECTION -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="INTRANET KETENGAH.">
    <meta name="keywords" content="intranet, ketengah">
    <meta name="author" content="ketengah">
    <title>
        @section('title')
            | INTRANET
        @show
    </title>
    <!--/ END META SECTION -->

    <!-- START @FAVICONS -->

    <!--/ END FAVICONS -->

<!-- START @FONT STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Oswald:700,400" rel="stylesheet">
    <!--/ END FONT STYLES -->

    <link href="{{ asset('blankon/global/plugins/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!--/ END GLOBAL MANDATORY STYLES -->

<!-- START @PAGE LEVEL STYLES -->
    <link href="{{ asset('blankon/global/plugins/bower_components/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('blankon/global/plugins/bower_components/animate.css/animate.min.css') }}" rel="stylesheet">
    @yield('header_styles')
    <!--/ END PAGE LEVEL STYLES -->

<!-- START @THEME STYLES -->
    <link href="{{ asset('blankon/admin/css/reset.css') }}" rel="stylesheet">
    <link href="{{ asset('blankon/admin/css/layout.css') }}" rel="stylesheet">
    <link href="{{ asset('blankon/admin/css/components.css') }}" rel="stylesheet">
    <link href="{{ asset('blankon/admin/css/plugins.css') }}" rel="stylesheet">
    <link href="{{ asset('blankon/admin/css/themes/default.theme.css') }}" rel="stylesheet" id="theme">

    @yield('extra_header_styles')

    <link href="{{ asset('blankon/admin/css/custom.css') }}" rel="stylesheet">
<!--/ END THEME STYLES -->




</head>

<body class="page-footer-fixed page-header-fixed page-sidebar-fixed">

<!--[if lt IE 9]>
<p class="upgrade-browser">Oppps!! You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- START @WRAPPER -->
<section id="wrapper">

<!-- START @HEADER -->
@include('admin.layouts.blankon.header')
@include('admin.layouts.blankon.sidebar_left')
@yield('content')
@include('admin.layouts.blankon.sidebar_right')
@if((Request::is('component','component/modals')))
    @include('component._all-modals')
@endif
</section>

<!-- START @BACK TOP -->
<div id="back-top" class="animated pulse circle">
    <i class="fa fa-angle-up"></i>
</div><!-- /#back-top -->
<!--/ END BACK TOP -->

<!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
<!-- START @CORE PLUGINS -->
<script src="{{ asset('blankon/global/plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('blankon/global/plugins/bower_components/jquery-cookie/jquery.cookie.js') }}"></script>
<script src="{{ asset('blankon/global/plugins/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('blankon/global/plugins/bower_components/typehead.js/dist/handlebars.js') }}"></script>
<script src="{{ asset('blankon/global/plugins/bower_components/typehead.js/dist/typeahead.bundle.min.js') }}"></script>
<script src="{{ asset('blankon/global/plugins/bower_components/jquery-nicescroll/jquery.nicescroll.min.js') }}"></script>
<script src="{{ asset('blankon/global/plugins/bower_components/jquery.sparkline.min/index.js') }}"></script>
<script src="{{ asset('blankon/global/plugins/bower_components/jquery-easing-original/jquery.easing.1.3.min.js') }}"></script>
<script src="{{ asset('blankon/global/plugins/bower_components/ionsound/js/ion.sound.min.js') }}"></script>
<script src="{{ asset('blankon/global/plugins/bower_components/bootbox/bootbox.js') }}"></script>
<!--/ END CORE PLUGINS -->

@yield('footer_scripts')

<!-- START @PAGE LEVEL SCRIPTS -->
<script src="{{ asset('blankon/admin/js/apps.js') }}"></script>
@yield('extra_footer_scripts')
<script src="{{ asset('blankon/admin/js/demo.js') }}"></script>


<!-- START GOOGLE ANALYTICS -->
<script>

</script>
<!--/ END GOOGLE ANALYTICS -->

</body>
<!--/ END BODY -->

</html>